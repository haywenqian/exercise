import * as React from "react";
import {Text, StyleSheet, Platform} from "react-native";

export const getFontSize = size => {
  switch (size) {
      case "xsmall" :
          return {fontSize: 11};
      case "small":
          return {fontSize: 12};
      case "medium" :
          return {fontSize: 13}
      case "large" :
          return {fontSize: 15};
      case "xlarge":
          return {fontSize: 18};
      case "xxlarge" :
          return {fontSize: 28};
      default:
          return getFontSize("medium");
  }
};

const CustomText = props =>{
  const {style, size, children, numLine} = props;
  return(
      <Text {...props} style={[styles.text, getFontSize(size), style]} allowFontScaling={false} numberOfLines={numLine}>
          {children}
      </Text>
  );
};

const styles = StyleSheet.create({
    text:{
        ...Platform.select({
            android: {

            }
        })
    }
});

export default CustomText;
